drop table if exists products;

create table products (
    id bigserial,
    name varchar,
    category varchar,
    color varchar,
    quantity int
);