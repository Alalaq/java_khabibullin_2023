package ru.itis.dto.converters;

import ru.itis.dto.ProductDto;
import ru.itis.dto.SignUpDto;

import javax.servlet.http.HttpServletRequest;

public class HttpFormsConverter {
    public static ProductDto from(HttpServletRequest request) {
        return ProductDto.builder()
                .name(request.getParameter("name"))
                .quantity(Integer.valueOf(request.getParameter("quantity")))
                .color(request.getParameter("color"))
                .category(request.getParameter("category"))
                .build();
    }
}
