package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String SIGN_UP_PATH = "/signUp";
    public static final String PRODUCTS_PATH = "/products";

    public static final String SEARCH_PAGE = "/liveSearch.html";

    public static final String USERS_SEARCH_PATH = "/liveSearch";
}
