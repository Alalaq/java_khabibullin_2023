package ru.itis.servlets;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;
import ru.itis.models.Product;
import ru.itis.services.ProductsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/search", name = "ProductSearchingServlet", loadOnStartup = 1)
public class ProductSearchingServlet extends HttpServlet {
    private ProductsService productsService;
    private HikariDataSource hikariDataSource;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getParameter("color") != null && req.getParameter("category") != null && req.getParameter("quantity") != null) {
            writer.println(getHtmlForProducts(productsService.getAllProducts()));
        } else {
            writer.println(Files.readString(Path.of("C:\\Users\\muzik\\Desktop\\java_khabibullin_2023\\Task11\\src\\main\\webapp\\search.html")));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Product> products;
        products = productsService.getAllProducts();
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        String html;

        List<Product> products_for_html = new ArrayList<>();

        if (request.getParameter("color").equals("") && request.getParameter("category").equals("") && request.getParameter("quantity").equals("")) {
            html = getHtmlForProducts(products);
            response.sendRedirect(request.getRequestURI() + "?quantity=any&category=any&color=any");

        } else {
            if (!request.getParameter("color").equals("") && request.getParameter("category").equals("") && request.getParameter("quantity").equals("")) {
                String color = request.getParameter("color");
                for (Product product : products) {
                    if (product.getColor().equals(color)) {
                        products_for_html.add(product);
                    }
                }
            } else if (request.getParameter("color").equals("") && !request.getParameter("category").equals("") && request.getParameter("quantity").equals("")) {
                String category = request.getParameter("category");

                for (Product product : products) {
                    if (product.getCategory().equals(category)) {
                        products_for_html.add(product);
                    }
                }
            } else if (request.getParameter("color").equals("") && request.getParameter("category").equals("") && !request.getParameter("quantity").equals("")) {
                Integer quantity = Integer.valueOf(request.getParameter("quantity"));


                for (Product product : products) {
                    if (product.getQuantity().equals(quantity)) {
                        products_for_html.add(product);
                    }
                }
            } else if (!request.getParameter("color").equals("") && !request.getParameter("category").equals("")&& request.getParameter("quantity").equals("")) {
                String color = request.getParameter("color");
                String category = request.getParameter("category");


                for (Product product : products) {
                    if (product.getCategory().equals(category) && product.getColor().equals(color)) {
                        products_for_html.add(product);
                    }
                }
            } else if (!request.getParameter("color").equals("") && request.getParameter("category").equals("") && !request.getParameter("quantity").equals("")) {
                String color = request.getParameter("color");
                Integer quantity = Integer.valueOf(request.getParameter("quantity"));


                for (Product product : products) {
                    if (product.getQuantity().equals(quantity) && product.getColor().equals(color)) {
                        products_for_html.add(product);
                    }
                }
            } else if (request.getParameter("color").equals("") && !request.getParameter("category").equals("") && !request.getParameter("quantity").equals("")) {
                String category = request.getParameter("category");
                Integer quantity = Integer.valueOf(request.getParameter("quantity"));


                for (Product product : products) {
                    if (product.getQuantity().equals(quantity) && product.getCategory().equals(category)) {
                        products_for_html.add(product);
                    }
                }
            } else if (!(request.getParameter("color").equals("") && request.getParameter("category").equals("") && request.getParameter("quantity").equals(""))) {
                String color = request.getParameter("color");
                String category = request.getParameter("category");
                Integer quantity = Integer.valueOf(request.getParameter("quantity"));


                for (Product product : products) {
                    if (product.getQuantity().equals(quantity) && product.getCategory().equals(category) && product.getColor().equals(color)) {
                        products_for_html.add(product);
                    }
                }
            }
            html = getHtmlForProducts(products_for_html);
        }
        writer.println(html);
    }

    private static String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Name</th>\n" +
                "\t\t<th>Category</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t\t<th>Quantity</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getCategory()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }

    @Override
    public void destroy() {
        hikariDataSource.close();
    }
}
