package ru.itis.servlets;


import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;
import ru.itis.models.Product;
import ru.itis.services.ProductsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@WebServlet(urlPatterns = "/add", name = "ProductAddingServlet", loadOnStartup = 1)
public class ProductAddingServlet extends HttpServlet {
    private ProductsService productsService;
    private HikariDataSource hikariDataSource;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        writer.println(Files.readString(Path.of("C:\\Users\\muzik\\Desktop\\java_khabibullin_2023\\Task11\\src\\main\\webapp\\success.html")));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String color = request.getParameter("color");
        Integer quantity = Integer.valueOf(request.getParameter("quantity"));
        productsService.signUp(name, quantity, color, category);

        response.sendRedirect( "/app/search?quantity=any&category=any&color=any");
    }

    private static String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>name</th>\n" +
                "\t\t<th>category</th>\n" +
                "\t\t<th>color</th>\n" +
                "\t\t<th>quantity</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getCategory()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }

    @Override
    public void destroy() {
        hikariDataSource.close();
    }
}
