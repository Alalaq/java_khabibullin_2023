package ru.itis.repositories;

import org.springframework.stereotype.Repository;
import ru.itis.models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    public static final String ANSI_RESET = "\u001B[0m";

    private static final String ANSI_RED = "\033[31m";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update products set name = :newName, quantity = :newQuantity," +
            "color = :newColor, category = :newCategory where id = :selectedId;";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from products where id = :selectedId";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from products;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from products where id = :id";

    //language=SQL
    private static final String SQL_FIND_ALL_QUANTITY_EXCEEDS = "select * from products where quantity > :minQuantity";

    //language=SQL
    private static final String SQL_LIKE_BY_NAME = "select * from products where " +
            "(products.name ilike :query)";

    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .quantity(row.getObject("quantity", Integer.class))
            .color(row.getString("color"))
            .category(row.getString("category"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        Map<String, Object> params = new HashMap<>();

        params.put("selectedId", product.getId());
        params.put("newName", product.getName());
        params.put("newQuantity", product.getQuantity());
        params.put("newColor", product.getColor());
        params.put("newCategory", product.getCategory());

        int rowsAffected = namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);

        if (rowsAffected != 1) {
            throw new IllegalArgumentException(ANSI_RED + "Couldn't update" + ANSI_RESET);
        }
    }

    @Override
    public boolean delete(Long id) {
        int rowsAffected = namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, Collections.singletonMap("selectedId", id));

        if (rowsAffected == 0) {
            System.out.println(ANSI_RED + "Could not delete." + ANSI_RESET);
            return false;
        }

        return true;

    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("quantity", product.getQuantity());
        paramsAsMap.put("color", product.getColor());
        paramsAsMap.put("category", product.getCategory());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("products")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();

        product.setId(id);
    }

    @Override
    public List<Product> findAllByNameOLike(String query) {
        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_NAME,
                Collections.singletonMap("query", "%" + query + "%"),
                productMapper);
    }

}
