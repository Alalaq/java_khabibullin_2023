package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDto;
import ru.itis.repositories.ProductsRepository;

import java.util.Collections;
import java.util.List;

import static ru.itis.dto.ProductDto.from;

@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> searchProducts(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return from(productsRepository.findAllByNameOLike(query));
    }
}
