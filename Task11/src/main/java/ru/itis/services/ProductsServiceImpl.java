package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService{

    private final ProductsRepository productsRepository;

    public void signUp(String name, Integer quantity, String color, String category) {
        Product product = Product.builder()
                .name(name)
                .category(category)
                .color(color)
                .quantity(quantity)
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

}
