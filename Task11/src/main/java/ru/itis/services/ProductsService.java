package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductsService {
    void signUp(String name, Integer quantity, String color, String category);

    abstract List<Product> getAllProducts();
}

