package ru.itis;

import ru.itis.Generator.SqlGenerator;
import ru.itis.Models.User;

public class Main {
    public static void main(String[] args) {
        User user = User.builder()
                .id(15L)
                .firstName("Aynur")
                .lastName("Sabitov")
                .isWorker(false)
                .build();

        SqlGenerator sqlGenerator = new SqlGenerator();
        System.out.println(sqlGenerator.createTable(user.getClass()));
        System.out.println(sqlGenerator.insert(user));
    }
}