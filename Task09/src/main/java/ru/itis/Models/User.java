package ru.itis.Models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import ru.itis.Annotations.ColumnName;
import ru.itis.Annotations.TableName;

@AllArgsConstructor
@Builder
@Data
@ToString
@TableName(tableName = "account")
public class User {
    @ColumnName(column = "id", type = "bigint", identity = true, primary = true)
    private Long id;
    @ColumnName(column = "firstName", maxLength = 25)
    private String firstName;
    @ColumnName(column = "lastName")
    private String lastName;
    @ColumnName(column = "isWorker", type = "boolean", defaultBoolean = true)
    private boolean isWorker;
}
