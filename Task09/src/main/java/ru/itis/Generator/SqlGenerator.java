package ru.itis.Generator;

import ru.itis.Annotations.ColumnName;
import ru.itis.Annotations.TableName;

import java.lang.reflect.Field;


public class SqlGenerator<T> {


    public String createTable(Class<T> entityClass) {
        String SQL_CREATE_TABLE = "create table ";
        String tableName = entityClass.getAnnotation(TableName.class).tableName();

        StringBuffer sb = new StringBuffer(SQL_CREATE_TABLE);
        sb.append(tableName).append("(");

        int count = 0;


        for (Field field : entityClass.getDeclaredFields()) {
            String name = field.getName();
            boolean primeKeyExists = false;

            ColumnName annotation = field.getAnnotation(ColumnName.class);
            if (entityClass.getDeclaredFields().length - 1 > count) {
                sb.append("\n ").append(name).append(" ").append(annotation.type()).append(annotation.maxLength() != -1 ? "(" + annotation.maxLength() + ")" : "");
                if (field.getAnnotation(ColumnName.class).identity()) {
                    sb.append(" GENERATED ALWAYS AS IDENTITY");
                }
                if (field.getAnnotation(ColumnName.class).primary()) {
                    sb.append(" primary key");
                    if (primeKeyExists){
                        throw new IllegalArgumentException();
                    }
                    else{
                        primeKeyExists = true;
                    }
                }
                if (field.getAnnotation(ColumnName.class).defaultBoolean() && field.getAnnotation(ColumnName.class).type().equals("boolean")) {
                    sb.append(" default true");
                }
                sb.append(", ");
                count++;
            } else {
                sb.append("\n ").append(name).append(" ").append(annotation.type()).append(");");
            }
        }


        return sb.toString();
    }

    public String insert(Object entity) {
        String SQL_INSERT = "insert into ";
        Class<T> tClass = (Class<T>) entity.getClass();
        String tableName = tClass.getAnnotation(TableName.class).tableName();

        StringBuffer sb = new StringBuffer(SQL_INSERT);
        sb.append(tableName + "(");

        int count = 0;
        for (Field field : tClass.getDeclaredFields()) {
            String columnName = field.getAnnotation(ColumnName.class).column();

            if (!field.getAnnotation(ColumnName.class).identity()) {
                if (count < tClass.getDeclaredFields().length - 1) {
                    sb.append(columnName + ", ");
                    count++;
                } else {
                    sb.append(columnName + ") values " + "\n(");
                }
            } else {
                count++;
            }
        }

        count = 0;

        for (Field field : tClass.getDeclaredFields()) {
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            field.setAccessible(true);
            if (!field.getAnnotation(ColumnName.class).identity()) {
                try {
                    if (count < tClass.getDeclaredFields().length - 1) {
                        if (field.getAnnotation(ColumnName.class).type().equals("varchar")) {
                            sb.append("\'" + field.get(entity) + "\'" + "::" + columnName.type() + ", ");
                        } else {
                            sb.append(field.get(entity)).append("::").append(columnName.type()).append(", ");
                        }

                        count++;
                    } else {
                        if (field.getAnnotation(ColumnName.class).type().equals("varchar")) {
                            sb.append("\'" + field.get(entity) + "\'" + "::" + columnName.type() + ");");
                        } else {
                            sb.append(field.get(entity) + "::" + columnName.type() + "");
                        }
                    }
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException();
                }
            } else {
                count++;
            }
            field.setAccessible(false);
        }
        sb.append(");");

        return sb.toString();
    }

}
