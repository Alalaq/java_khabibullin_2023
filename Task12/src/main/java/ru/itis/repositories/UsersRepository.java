package ru.itis.repositories;

import ru.itis.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    Optional<User> findById(Long id);

    void update(User user);

    void delete(Long id);

    Optional<User> findOneByEmail(String email);
}
