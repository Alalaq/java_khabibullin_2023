package ru.itis.security;

public interface AuthenticationManager  {
    boolean authenticate(String email, String password);
}
