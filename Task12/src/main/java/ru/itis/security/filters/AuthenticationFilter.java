package ru.itis.security.filters;

import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import ru.itis.security.AuthenticationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.itis.constants.Paths.*;


@Order(2)
@WebFilter(urlPatterns = "/*", filterName = "AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(APPLICATION_PREFIX + AUTHENTICATION_PAGE) || request.getRequestURI().equals(APPLICATION_PREFIX + REGISTRATION_PAGE)) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpSession session = request.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                filterChain.doFilter(request, response);
                return;
            }
        }
        response.sendRedirect(APPLICATION_PREFIX + AUTHENTICATION_PAGE);
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
