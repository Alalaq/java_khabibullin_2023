package ru.itis.security.filters;

import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.itis.constants.Paths.*;
import static ru.itis.servlets.ProductsSearchServlet.getCookieByName;

@Order(0)
@WebFilter(urlPatterns = "/*", filterName = "URLFilter")
public class URLFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(APPLICATION_PREFIX + AUTHENTICATION_PAGE) || request.getRequestURI().equals(APPLICATION_PREFIX + REGISTRATION_PAGE) || request.getRequestURI().equals(APPLICATION_PREFIX + "/signIn") || request.getSession(false) != null) {
            filterChain.doFilter(request, response);
            return;

        } else if (request.getSession(false) == null && (!request.getRequestURI().equals(APPLICATION_PREFIX + "/"))){
            Cookie cookie = getCookieByName(request.getCookies(), "pageWanted");
            if (cookie != null){
                cookie.setValue(request.getRequestURI());
            }
            else{
                response.addCookie(new Cookie("pageWanted", request.getRequestURI()));
                response.sendRedirect(APPLICATION_PREFIX + AUTHENTICATION_PAGE);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
