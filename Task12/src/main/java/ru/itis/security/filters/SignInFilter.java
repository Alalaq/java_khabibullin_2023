package ru.itis.security.filters;

import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import ru.itis.security.AuthenticationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.itis.constants.Paths.*;
import static ru.itis.servlets.ProductsSearchServlet.getCookieByName;

@Order(1)
@WebFilter(urlPatterns = {AUTHENTICATION_PATH, AUTHENTICATION_PAGE}, filterName = "SignInFilter")
public class SignInFilter implements Filter {


    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if (session == null) {

            if (request.getMethod().equals("POST")) {
                String email = request.getParameter("email");
                String password = request.getParameter("password");

                if (authenticationManager.authenticate(email, password)) {
                    session = request.getSession(true);
                    session.setAttribute("authenticated", true);
                    if (getCookieByName(request.getCookies(), "pageWanted") != null) {
                        response.sendRedirect(getCookieByName(request.getCookies(), "pageWanted").getValue());
                    } else {
                        response.sendRedirect(APPLICATION_PREFIX + PRODUCTS_LIVESEARCH_PAGE);
                    }
                } else {
                    response.sendRedirect(APPLICATION_PREFIX + AUTHENTICATION_PAGE + "?error");
                }
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }

        else {
            response.sendRedirect(APPLICATION_PREFIX + PROFILE_PAGE);
        }
    }

    @Override
    public void destroy() {
    }
}
