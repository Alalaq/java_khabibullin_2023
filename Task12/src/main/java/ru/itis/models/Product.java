package ru.itis.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Product {
    private Long id;
    private String name;
    private Integer quantity;
    private String color;
    private String category;

    public Product(String name, Integer quantity, String color, String category){
        this.name = name;
        this.quantity = quantity;
        this.color = color;
        this.category = category;
    }
}
