package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDto;
import ru.itis.dto.SignUpDtoForProducts;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductsService;

import java.util.List;

import static ru.itis.dto.ProductDto.from;

@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public boolean signUp(SignUpDtoForProducts signUpData) {
        try {
            Product product = Product.builder()
                    .name(signUpData.getName())
                    .category(signUpData.getCategory())
                    .color(signUpData.getColor())
                    .quantity(signUpData.getQuantity())
                    .build();

            productsRepository.save(product);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return from(productsRepository.findAll());
    }

}
