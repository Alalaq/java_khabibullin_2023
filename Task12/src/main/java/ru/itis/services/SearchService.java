package ru.itis.services;

import ru.itis.dto.ProductDto;

import java.util.List;

public interface SearchService {
    List<ProductDto> searchProductsLike(String query);

    List<ProductDto> searchProductsSortedBy(String parameter);
}
