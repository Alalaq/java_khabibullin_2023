package ru.itis.services;

import ru.itis.dto.ProductDto;
import ru.itis.dto.SignUpDtoForProducts;

import java.util.List;

public interface ProductsService {
    boolean signUp(SignUpDtoForProducts signUpData);

    abstract List<ProductDto> getAllProducts();
}

