package ru.itis.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

import static ru.itis.constants.Paths.APPLICATION_PREFIX;
import static ru.itis.constants.Paths.PRODUCTS_SEARCH_PATH;

@WebServlet(urlPatterns = "/profileSettings", name = "ProductFiltersServlet")
public class ProductFiltersServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("logout") != null){
            request.logout();
            HttpSession session = request.getSession();
            session.invalidate();
        }
        
        String parameter = request.getParameter("sortParameter");
        Cookie sortingParameterCookie = new Cookie("sortParameter", parameter);
        sortingParameterCookie.setPath("/");
        response.addCookie(sortingParameterCookie);
        response.sendRedirect(APPLICATION_PREFIX + PRODUCTS_SEARCH_PATH);
    }
}
