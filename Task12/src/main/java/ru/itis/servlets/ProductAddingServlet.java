package ru.itis.servlets;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;
import ru.itis.dto.ProductDto;
import ru.itis.dto.SignUpDtoForProducts;
import ru.itis.models.Product;
import ru.itis.services.ProductsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ru.itis.constants.Paths.PRODUCTS_PATH;

@WebServlet(urlPatterns = {PRODUCTS_PATH}, name = "ProductAddingServlet", loadOnStartup = 1)
public class ProductAddingServlet extends HttpServlet {
    private ProductsService productsService;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.productsService = context.getBean(ProductsService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        PrintWriter writer = response.getWriter();
//        writer.println(Files.readString(Path.of("C:\\Users\\muzik\\Desktop\\java_khabibullin_2023\\app\\src\\main\\webapp\\success.html")));
//    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //if (request.getRequestURI().equals(APPLICATION_PREFIX + USERS_PATH)) {
            String body = request.getReader().readLine();
            SignUpDtoForProducts productData = objectMapper.readValue(body, SignUpDtoForProducts.class);
            productsService.signUp(productData);
            List<ProductDto> products = productsService.getAllProducts();
            String jsonResponse = objectMapper.writeValueAsString(products);
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse);
      //  }
       // else {
       //     response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        //}

    }

    private static String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>name</th>\n" +
                "\t\t<th>category</th>\n" +
                "\t\t<th>color</th>\n" +
                "\t\t<th>quantity</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getCategory()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }
}
