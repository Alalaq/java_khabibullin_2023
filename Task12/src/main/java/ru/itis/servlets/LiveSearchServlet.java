package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.ProductDto;
import ru.itis.services.SearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ru.itis.constants.Paths.PRODUCTS_LIVESEARCH_PATH;

@WebServlet(name = "searchServlet", urlPatterns = {PRODUCTS_LIVESEARCH_PATH}, loadOnStartup = 1)
public class LiveSearchServlet extends HttpServlet {
    private SearchService searchService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        List<ProductDto> products = searchService.searchProductsLike(query);
        String jsonResponse = objectMapper.writeValueAsString(products);
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);
    }
}
