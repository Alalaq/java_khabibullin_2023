package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.ProductDto;
import ru.itis.services.SearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.itis.constants.Paths.PRODUCTS_SEARCH_PATH;

@WebServlet(name = "productsSearchServlet", urlPatterns = {PRODUCTS_SEARCH_PATH}, loadOnStartup = 1)
public class ProductsSearchServlet extends HttpServlet {
    private SearchService searchService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<ProductDto> products;
        Cookie[] parameters = request.getCookies();
        Cookie parameterCookie = getCookieByName(parameters, "sortParameter");
        String parameter = parameterCookie.getValue();


        products = searchService.searchProductsSortedBy(parameter);

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

        String html = getHtmlForProducts(products);

        writer.println(html);

    }

    public static String getHtmlForProducts(List<ProductDto> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Products</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Product Name</th>\n" +
                "\t\t<th>Category</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t\t<th>Quantity</th>\n" +
                "\t</tr>");

        for (ProductDto product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getCategory()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }


    public static Cookie getCookieByName(Cookie[] cookies, String name){
        Cookie cookie = null;
        for (Cookie cookie1 : cookies){
            if (cookie1.getName().equals(name)){
                cookie = cookie1;
            }
        }
        return cookie;
    }
}
