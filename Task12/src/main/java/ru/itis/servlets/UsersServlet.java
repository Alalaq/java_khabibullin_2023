package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.dto.converters.http.HttpFormsConverter;
import ru.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {REGISTRATION_PATH}, loadOnStartup = 1)
public class UsersServlet extends HttpServlet {
    private UsersService usersService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getRequestURI().equals(APPLICATION_PREFIX + REGISTRATION_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.from(request);
            usersService.signUp(signUpData);
            response.sendRedirect(APPLICATION_PREFIX + PRODUCTS_LIVESEARCH_PAGE);
        } else if (request.getRequestURI().equals(APPLICATION_PREFIX + USERS_PATH)) {
            String body = request.getReader().readLine();
            SignUpDto userData = objectMapper.readValue(body, SignUpDto.class);
            usersService.signUp(userData);
            List<UserDto> users = usersService.getAllUsers();
            String jsonResponse = objectMapper.writeValueAsString(users);
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
