package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";
    public static final String REGISTRATION_PATH = "/registration";
    public static final String AUTHENTICATION_PATH = "/authentication";
    public static final String USERS_PATH = "/users";
    public static final String PRODUCTS_SEARCH_PATH = "/products_search";
    public static final String PRODUCTS_PATH = "/products";
    public static final String PRODUCTS_LIVESEARCH_PATH = "/liveSearch";

    public static final String PRODUCTS_PAGE = "/products/products.html";
    public static final String PRODUCTS_LIVESEARCH_PAGE = "/products/liveSearch.html";
    public static final String PROFILE_PAGE = "/products/profile.html";

    public static final String AUTHENTICATION_PAGE = "/access/authentication.html";


    public static final String REGISTRATION_PAGE = "/access/registration.html";


}
