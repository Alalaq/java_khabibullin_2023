function searchProducts(query) {
    return fetch('/app/liveSearch?query=' + query)
        .then((response) => {
            return response.json()
        }).then((products) => {
            fillTable(products)
        })
}

function fillTable(products) {
    let table = document.getElementById("productsTable");

    table.innerHTML = '    <tr>\n' +
        '        <th>id</th>\n' +
        '        <th>Name</th>\n' +
        '        <th>Quantity</th>\n' +
        '        <th>Color</th>\n' +
        '        <th>Category</th>\n' +
        '    </tr>';

    for (let i = 0; i < products.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let quantityCell = row.insertCell(2);
        let colorCell = row.insertCell(3);
        let categoryCell = row.insertCell(4);

        idCell.innerHTML = products[i].id;
        nameCell.innerHTML = products[i].name;
        quantityCell.innerHTML = products[i].quantity;
        colorCell.innerHTML = products[i].color;
        categoryCell.innerHTML = products[i].category;
    }
}

function addProduct(name, category, color, quantity) {
    let body = {
        "name": name,
        "category": category,
        "color": color,
        "quantity": quantity
    };

    fetch('/app/products', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((products) => fillTable(products))
        .catch((error) => {
            alert(error)
        })
}

function findSession() {
    return document.cookie
        .split('; ')
        .find((row) => row.startsWith("JSESSIONID"));
}

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
