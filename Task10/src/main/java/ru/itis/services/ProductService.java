package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    boolean addProduct(Product product);

    List<Product> getListOfAllProducts();

    boolean deleteProduct(Long id);
}
