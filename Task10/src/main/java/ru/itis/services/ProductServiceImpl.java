package ru.itis.services;

import org.springframework.dao.DataIntegrityViolationException;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;

import java.util.List;

public class ProductServiceImpl implements ProductService{
    private final ProductsRepository productsRepository;

    public ProductServiceImpl(ProductsRepository productsRepository){
        this.productsRepository = productsRepository;
    }

    @Override
    public boolean addProduct(Product product) {
        try {
            productsRepository.save(product);
            return true;
        } catch (DataIntegrityViolationException ex) {
            return false;
        }

    }

    @Override
    public List<Product> getListOfAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id);
    }
}
