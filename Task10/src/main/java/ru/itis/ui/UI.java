package ru.itis.ui;

import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UI {
    public static final String ANSI_RESET = "\u001B[0m";

    private static final String ANSI_RED = "\033[31m";

    private final Scanner scanner = new Scanner(System.in);

    private final ProductService productService;

    public UI(ProductService productService){
        this.productService = productService;
    }

    public void start(){
        while (true){
            printMainMenu();

            String command = scanner.nextLine();

            switch (command) {
                case "1" -> {
                    System.out.println("You've selected the adding of product. ");
                    addAProduct();
                }
                case "2" -> {
                    System.out.println("Here's the list of all products: ");
                    getListOfProducts();
                }
                case "3" -> {
                    System.out.println("You've selected the removal of a product. ");
                    deleteAProduct();
                    scanner.nextLine();
                }
                case "4" -> {
                    System.out.println("Exiting the program...");
                    System.exit(0);
                }
                default -> System.out.println("Sorry, there's no such command. Try again.");
            }
        }
    }

    private void addAProduct(){
        System.out.println("Enter the product specifications: ");

        System.out.println();

        System.out.println("Enter the name of a product: ");
        String name = scanner.nextLine();

        int quantity = -1;

        while (quantity == -1) {
            System.out.println("Enter the quantity of a product: ");
            try {
                quantity = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println(ANSI_RED + "You should enter the number. " + ANSI_RESET);
                scanner.nextLine();
            }
        }

        scanner.nextLine();

        System.out.println("Enter the color of a product: ");
        String color = scanner.nextLine();


        System.out.println("Enter the category of a product: ");
        String category = scanner.nextLine();

        Product product = new Product(name, quantity, color, category);

        if (productService.addProduct(product)){
            System.out.println("Product has been successfully added to the database.");
        }
        else{
            System.out.println(ANSI_RED + "Could not add product to the database." + ANSI_RESET);
        }
    }

    private void deleteAProduct(){
        Long id = -1;

        while (id == -1) {
            System.out.println("Enter the product's ID: ");
            try {
                id = scanner.nextLong();
            } catch (InputMismatchException ex) {
                System.out.println(ANSI_RED + "You should enter the number. " + ANSI_RESET);
                scanner.nextLine();
            }
        }
        if (productService.deleteProduct(id)){
            System.out.println("Product successfully deleted");
        }
    }

    private void getListOfProducts(){
        List<Product> products = productService.getListOfAllProducts();

        for (Product product : products){
            System.out.println(product.toString());
        }
    }

    private void printMainMenu(){
        System.out.println();
        System.out.println("Choose an option: ");
        System.out.println("1. Add new product;");
        System.out.println("2. Get the list of products;");
        System.out.println("3. Delete a product;");
        System.out.println("4. Exit.");
    }
}
