package main.java.ru.itis.repositories;

import main.java.ru.itis.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    Optional<Product> findById(Long id);

    void update(Product product);

    void delete(Long id);

    List<Product> findAll();

    void save(Product product);

    List<Product> findAllQuantityExceeds(int minQuantity);
}
