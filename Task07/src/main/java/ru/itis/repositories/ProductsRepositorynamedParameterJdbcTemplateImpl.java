package main.java.ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import main.java.ru.itis.models.Product;

import java.util.*;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

public class ProductsRepositorynamedParameterJdbcTemplateImpl implements ProductsRepository
{
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update product set name = :newName, quantity = :newQuantity," +
            "color = :newColor, category = :newCategory where id = :selectedId;";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = :selectedId";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = :id";

    //language=SQL
    private static final String SQL_FIND_ALL_QUANTITY_EXCEEDS = "select * from product where quantity > :minQuantity";

    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .quantity(row.getObject("quantity", Integer.class))
            .color(row.getString("color"))
            .category(row.getString("category"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositorynamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productMapper));
        }
        catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        Map<String, Object> params = new HashMap<>();

        params.put("selectedId", product.getId());
        params.put("newName", product.getName());
        params.put("newQuantity", product.getQuantity());
        params.put("newColor", product.getColor());
        params.put("newCategory", product.getCategory());

        int rowsAffected = namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);

        if (rowsAffected != 1){
            throw new IllegalArgumentException("Couldn't update");
        }
    }

    @Override
    public void delete(Long id) {
        int rowsAffected = namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, Collections.singletonMap("selectedId", id));

        if (rowsAffected == 0) {
            throw new IllegalArgumentException("Couldn't delete");
        }

    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", product.getId());
        paramsAsMap.put("quantity", product.getId());
        paramsAsMap.put("color", product.getId());
        paramsAsMap.put("category", product.getId());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();

        product.setId(id);
    }

    @Override
    public List<Product> findAllQuantityExceeds(int minQuantity){
        return namedParameterJdbcTemplate.query(SQL_FIND_ALL_QUANTITY_EXCEEDS,
                Collections.singletonMap("minQuantity", minQuantity),
                productMapper);
    }
}
