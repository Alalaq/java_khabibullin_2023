package main.java.ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import main.java.ru.itis.models.Product;
import main.java.ru.itis.repositories.ProductsRepository;
import main.java.ru.itis.repositories.ProductsRepositorynamedParameterJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;


public class Tests {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try{
            properties.load(Tests.class.getResourceAsStream("/db.properties"));
        }
        catch (IOException e){
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(17);

        HikariDataSource dataSource = new HikariDataSource(config);
        ProductsRepository repository = new ProductsRepositorynamedParameterJdbcTemplateImpl(dataSource);

        Product product = new Product();
        Product product2 = new Product();

        product.builder().id(6L)
                .name("cup")
                .quantity(500)
                .color("black")
                .category("home")
                .build();

        product2.builder()
                .name("carpet")
                .quantity(2414)
                .color("white")
                .category("home")
                .build();

        repository.save(product2);

       // repository.update(product);
    }
}
